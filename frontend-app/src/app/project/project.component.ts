import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Project} from "../models/project.model";
import {ProjectService} from "../services/project.service";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  constructor(private router: Router, private projectService: ProjectService) { }

  projects: Project[];

  ngOnInit() {
    this.projectService.getProjects()
      .subscribe( data => {
        this.projects = data;
      });
  };

  deleteProject(project: Project): void {
    this.projectService.deleteProject(project)
      .subscribe( data => {
        this.projects = this.projects.filter(u => u !== project);
      })
  };

}
