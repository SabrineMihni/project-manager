import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {TaskService} from "../services/task.service";
import {Task} from "../models/task.model";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  constructor(private router: Router, private taskService: TaskService) { }

  tasks: Task[];

  ngOnInit() {
    this.taskService.getTasks()
      .subscribe( data => {
        this.tasks = data;
      });
  };

  deleteTask(task: Task): void {
    this.taskService.deleteTask(task)
      .subscribe( data => {
        this.tasks = this.tasks.filter(u => u !== task);
      })
  };


}
