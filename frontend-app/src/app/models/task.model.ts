export class Task {

  id: string;
  name: string;
  description: string;
  days_spent: object;
  created_at: object;
  finished_at: object;
}
