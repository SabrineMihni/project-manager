export class Project {

  id: string;
  title: string;
  description: string;
  created_at: object;
  finished_at: object;
  tasks: object;
}
