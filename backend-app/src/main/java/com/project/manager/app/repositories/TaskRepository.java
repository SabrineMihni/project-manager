package com.project.manager.app.repositories;

import com.project.manager.app.models.Task;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface TaskRepository extends Repository<Task, Integer> {

    Task delete(Task task);

    List<Task> findAll();

    Task findOne(int id);

    Task save(Task task);

    Task update(Task task);
}
