package com.project.manager.app.controllers;

import com.project.manager.app.models.Project;
import com.project.manager.app.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/api/projects"})
public class ProjectController  {

    @Autowired
    private ProjectService projectService;

    @PostMapping
    public Project create(@RequestBody Project project) {
        return projectService.create(project);
    }

    @GetMapping(path = {"/{id}"})
    public Project findOne(@PathVariable("id") int id){
        return projectService.findById(id);
    }

    @PutMapping
    public Project update(@RequestBody Project project){
        return projectService.update(project);
    }

    @DeleteMapping(path = {"/{id}"})
    public Project delete(@PathVariable("id") int id){
        return projectService.delete(id);
    }

    @GetMapping
    public List<Project> findAll() {
        return projectService.findAll();
    }

}
