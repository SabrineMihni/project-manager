package com.project.manager.app.services;


import com.project.manager.app.models.Task;

import java.util.List;

public interface TaskService {

    Task create(Task task);

    Task delete(Task task);

    List<Task> findAll();

    Task findById(int id);

    Task update(Task task);
}