package com.project.manager.app.controllers;

import com.project.manager.app.models.Task;
import com.project.manager.app.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/api/tasks"})
public class TaskController  {

    @Autowired
    private TaskService taskService;

    @PostMapping
    public Task create(@RequestBody Task task) {
        return taskService.create(task);
    }

    @GetMapping(path = {"/{id}"})
    public Task findOne(@PathVariable("id") int id){
        return taskService.findById(id);
    }

    @PutMapping
    public Task update(@RequestBody Task task){
        return taskService.update(task);
    }

    @DeleteMapping(path = {"/{id}"})
    public Task delete(@PathVariable("id") int id){
        return taskService.delete(id);
    }

    @GetMapping
    public List<Task> findAll() {
        return taskService.findAll();
    }

}