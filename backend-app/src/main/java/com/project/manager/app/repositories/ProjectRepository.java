package com.project.manager.app.repositories;

import com.project.manager.app.models.Project;
import com.project.manager.app.services.ProjectService;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface ProjectRepository extends Repository<Project, Integer> {

    void delete(Project project);

    List<Project> findAll();

    Project findOne(int id);

    Project save(Project project);

    Project update(Project project);

}
