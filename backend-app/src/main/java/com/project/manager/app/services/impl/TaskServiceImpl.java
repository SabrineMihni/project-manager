package com.project.manager.app.services.impl;

import com.project.manager.app.models.Task;
import com.project.manager.app.repositories.TaskRepository;
import com.project.manager.app.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.Repository;
import java.util.List;


public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository repository;

    @Override
    public Task create(Task task) {
        return repository.save(task);
    }

    @Override
    public Task delete(Task task) {
        return repository.delete(task);
    }

    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    public Task findById(int id) {
        return repository.findOne(id);
    }

    @Override
    public Task update(Task task) {
        return repository.update(task);
    }
}