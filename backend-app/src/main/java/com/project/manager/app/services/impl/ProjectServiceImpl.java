package com.project.manager.app.services.impl;

import com.project.manager.app.models.Project;
import com.project.manager.app.repositories.ProjectRepository;
import com.project.manager.app.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.Repository;
import java.util.List;


public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository repository;

    @Override
    public Project create(Project project) {
        return repository.save(project);
    }

    @Override
    public Project delete(int id) {
        Project project = findById(id);
        if( project != null ) {
            repository.delete(project);
        }
        return project;
    }

    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @Override
    public Project findById(int id) {
        return repository.findOne(id);
    }

    public Project update(Project project) {
        return repository.update(project);
    }
}